package application;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class Controller {
	
	private Stage stage;
	private FileChooser fileChooser;
	private static File selectedFile;
	private Image targetImage;
	
	private String path;
	private String mdpi;
	private String hdpi;
	private String xhdpi;
	private String xxhdpi;
	private String xxxhdpi;
	
	 public static final int SCALE_AREA_AVERAGING = 16;
	 public static final int SCALE_DEFAULT = 1;
	 public static final int SCALE_FAST = 2;
	 public static final int SCALE_REPLICATE = 8;
	 public static final int SCALE_SMOOTH = 4;
	
	public void setStage(Stage stage){
		this.stage = stage;
	}
	
	@FXML
	private ProgressIndicator progress;
	public void getTargetImage(ActionEvent event) {
	    this.fileChooser = new FileChooser();
	    
	    this.setFileChooserFilter();
	    
		this.selectedFile = this.fileChooser.showOpenDialog(stage);
		
		progress.setProgress(10);
		
		this.setImage();
	}
	
	private void setFileChooserFilter() {
		FileChooser.ExtensionFilter imageFilter
	    = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png", "*.jpeg" , "*.bmp");
		this.fileChooser.getExtensionFilters().add(imageFilter);
	}
	
	
	@FXML
    private ImageView imageView;
	private void setImage() {
		
		progress.setProgress(20);
		
		BufferedImage bufferdImage = this.getScalledBufferedImage((int)imageView.getFitWidth(),
				(int)imageView.getFitHeight());
		
		progress.setProgress(60);
			
		Image newImage = SwingFXUtils.toFXImage( bufferdImage, null);
		
		progress.setProgress(80);
		
        imageView.setImage(newImage);
        
        progress.setProgress(100);
        
        final Task<Void> task = new Task<Void>() {
            final int N_ITERATIONS = 10;

            @Override
            protected Void call() throws Exception {
                for (int i = 0; i < N_ITERATIONS; i++) {
                    updateProgress(i + 1, N_ITERATIONS);
                    // sleep is used to simulate doing some work which takes some time....
                    Thread.sleep(10);
                }

                return null;
            }
        };

        
        progress.progressProperty().bind(
                task.progressProperty()
        );
        // color the bar green when the work is complete.
        progress.progressProperty().addListener(observable -> {
            if (progress.getProgress() >= 1 - 0.0000005) {
                progress.setStyle("-fx-accent: forestgreen;");
            }
        });
        
	}	
	
	private BufferedImage getScalledBufferedImage(int width , int height) {
		
		progress.setProgress(30);
		
		BufferedImage bimage = null;
		try {
			bimage = ImageIO.read(this.selectedFile);		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		progress.setProgress(40);

		java.awt.Image g = bimage.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
	
		progress.setProgress(60);
		
		BufferedImage temp = this.toBufferedImage(g);
		
		return temp;
	}
	
	private static BufferedImage toBufferedImage(java.awt.Image src) {
        int w = src.getWidth(null);
        int h = src.getHeight(null);
        int type = BufferedImage.TYPE_INT_RGB;  // other options
        BufferedImage dest = new BufferedImage(w, h, type);
        Graphics2D g2 = dest.createGraphics();
        g2.drawImage(src, 0, 0, null);
        g2.dispose();
        return dest;
    }
	
	@FXML 
	private CheckBox jpg;
	@FXML 
	private CheckBox jpeg;
	@FXML 
	private CheckBox png;
	@FXML 
	private CheckBox bmp;
	public void androidButtonGenerator() {
		
		progress.setProgress(0);
		
		this.setPaths();
		
		progress.setProgress(10);
		
		new File(this.path).mkdirs();
		new File(this.mdpi).mkdirs();
		new File(this.hdpi).mkdirs();
		new File(this.xhdpi).mkdirs();
		new File(this.xxhdpi).mkdirs();
		new File(this.xxxhdpi).mkdirs();
		
		progress.setProgress(40);
		
		if(jpg.isSelected()) {
			this.detectExtension("jpg");
		}
		if(jpeg.isSelected()) {
			this.detectExtension("jpeg");
		}
		if(png.isSelected()) {
			this.detectExtension("png");
		}
		if(bmp.isSelected()) {
			this.detectExtension("bmp");
		}
		
		progress.setProgress(100);
		
	}
	
	private void setPaths() {
		this.path = this.selectedFile.getParent() + "\\Androider";
		this.mdpi = path + "\\drawable-mdpi";
		this.hdpi = path + "\\drawable-hdpi";
		this.xhdpi = path + "\\drawable-xhdpi";
		this.xxhdpi = path + "\\drawable-xxhdpi";
		this.xxxhdpi = path + "\\drawable-xxxhdpi";
	}
	
	private void detectExtension(String extension) {
		BufferedImage bufferdImage = this.getScalledBufferedImage(48 , 48);
		this.save(bufferdImage, extension, mdpi);
		
		bufferdImage = this.getScalledBufferedImage(72 , 72);
		this.save(bufferdImage, extension, hdpi);
		
		bufferdImage = this.getScalledBufferedImage(96 , 96);
		this.save(bufferdImage, extension, xhdpi);
		
		bufferdImage = this.getScalledBufferedImage(144 , 144);
		this.save(bufferdImage, extension, xxhdpi);
		
		bufferdImage = this.getScalledBufferedImage(192 , 192);
		this.save(bufferdImage, extension, xxxhdpi);
	}
	
	private static void save(BufferedImage image, String ext , String path) {
        String fileName = "Androider";
        File file = new File( path + "\\" + fileName + "." + ext);
        try {
            ImageIO.write(image, ext, file);  // ignore returned boolean
        } catch(IOException e) {
            System.out.println("Write error for " + file.getPath() +
                               ": " + e.getMessage());
        }
    }
}
